import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


import { PeopleService } from 'app/dashboard/services/people/people.service';
import { Observable, of } from 'rxjs';
import { SearchType } from 'app/shared/models/search-type';
import { PlanetsService } from 'app/dashboard/services/planets/planets.service';
import { FilmsService } from 'app/dashboard/services/films/films.service';
import { SpeciesService } from 'app/dashboard/services/species/species.service';
import { StarshipsService } from 'app/dashboard/services/starships/starships.service';
import { VehiclesService } from 'app/dashboard/services/vehicles/vehicles.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {
  itemList$: Observable<any> = of([]);

  searchText: string;

  searchType: SearchType = SearchType.People;
  searchTypesList = SearchType;
  searchTypesOptions = [
    'People',
    'Films',
    'Planets',
    'Species',
    'Starships',
    'Vehicles'
  ];

  constructor(
    private router: Router,
    private peopleService: PeopleService,
    private planetsService: PlanetsService,
    private filmsService: FilmsService,
    private speciesService: SpeciesService,
    private starshipsService: StarshipsService,
    private vehiclesService: VehiclesService
  ) { }

  ngOnInit() {
    this.setSearchType();
  }

  moreInfo(id) {
    const idNum = id.replace(/[^0-9]/g, '');
    const typeSW = id.split(/(\/)/u);
    this.itemList$ = this.peopleService.fetchPeopleId(idNum);
    this.router.navigate(['/details', typeSW[8], idNum]);
  }

  setSearchType() {
    switch (this.searchType) {
      case SearchType.Films:
        this.itemList$ = this.filmsService.fetchFilmsList();
        break;

      case SearchType.Planets:
        this.itemList$ = this.planetsService.fetchPlanetsList();
        break;

      case SearchType.Species:
        this.itemList$ = this.speciesService.fetchSpeciesList();
        break;

      case SearchType.Starships:
        this.itemList$ = this.starshipsService.fetchStarshipsList();
        break;

      case SearchType.Vehicles:
        this.itemList$ = this.vehiclesService.fetchVehiclesList();
        break;

      case SearchType.People:
      default:
        this.itemList$ = this.peopleService.fetchPeopleList();
        break;
    }
  }

}
