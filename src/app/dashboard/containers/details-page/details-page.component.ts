import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { PeopleService } from 'app/dashboard/services/people/people.service';
import { PlanetsService } from 'app/dashboard/services/planets/planets.service';
import { FilmsService } from 'app/dashboard/services/films/films.service';
import { SpeciesService } from 'app/dashboard/services/species/species.service';
import { StarshipsService } from 'app/dashboard/services/starships/starships.service';
import { VehiclesService } from 'app/dashboard/services/vehicles/vehicles.service';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit {
  item$: Observable<any> = of({});
  id: string;
  typeSW: string;
  info: any;

  constructor(
    private route: ActivatedRoute,
    private peopleService: PeopleService,
    private planetsService: PlanetsService,
    private filmsService: FilmsService,
    private speciesService: SpeciesService,
    private starshipsService: StarshipsService,
    private vehiclesService: VehiclesService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((param: any) => {
      this.id = param.params.id;
      this.typeSW = param.params.type;
    });
    this.setSearchType();
  }

  setSearchType() {
    switch (this.typeSW.toLowerCase()) {
      case 'films':
        this.item$ = this.filmsService.fetchFilmseId(this.id);
        break;

      case 'planets':
        this.item$ = this.planetsService.fetchPlanetsId(this.id);
        break;

      case 'species':
        this.item$ = this.speciesService.fetchSpeciesId(this.id);
        break;

      case 'starships':
        this.item$ = this.starshipsService.fetchStarshipsById(this.id);
        break;

      case 'vehicles':
        this.item$ = this.vehiclesService.fetchVehiclesId(this.id);
        break;

      case 'people':
      default:
        this.item$ = this.peopleService.fetchPeopleId(this.id);
        break;
    }
  }
}
