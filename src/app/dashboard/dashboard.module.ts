import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'app/shared/shared.module';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SearchPageComponent } from './containers/search-page/search-page.component';
import { DetailsPageComponent } from './containers/details-page/details-page.component';

@NgModule({
  declarations: [
    DashboardComponent,
    SearchPageComponent,
    DetailsPageComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule
  ]
})
export class DashboardModule { }
