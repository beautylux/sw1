import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchPageComponent } from './containers/search-page/search-page.component';
import { DetailsPageComponent } from './containers/details-page/details-page.component';

const routes: Routes = [
  {
    path: '',
    component: SearchPageComponent
  },
  {
    path: 'details/:type/:id',
    component: DetailsPageComponent,
    data: { type: 'type' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
