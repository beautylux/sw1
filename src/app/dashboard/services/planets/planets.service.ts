import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from 'app/shared/services/api/api.service';

import {httpPlanetById, httpPlanetsList} from 'app/shared/utils/http.helper';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  constructor(
    private apiService: ApiService
  ) { }

  fetchPlanetsList(): Observable<any> {
    return this.apiService.get(httpPlanetsList());
  }

  fetchPlanetsId(id): Observable<any> {
    return this.apiService.get(httpPlanetById(id));
  }
}
