import { Injectable } from '@angular/core';

import { ApiService } from 'app/shared/services/api/api.service';

import { httpPeopleList, httpPeopleById } from 'app/shared/utils/http.helper';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(
    private apiService: ApiService
  ) { }

  fetchPeopleList(): Observable<any> {
    return this.apiService.get(httpPeopleList());
  }
  fetchPeopleId(id): Observable<any> {
    return this.apiService.get(httpPeopleById(id));
  }
}
