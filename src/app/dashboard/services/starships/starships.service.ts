import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from 'app/shared/services/api/api.service';

import {httpStarshipById, httpStarshipsList} from 'app/shared/utils/http.helper';

@Injectable({
  providedIn: 'root'
})
export class StarshipsService {

  constructor(
    private apiService: ApiService
  ) { }

  fetchStarshipsList(): Observable<any> {
    return this.apiService.get(httpStarshipsList());
  }

  fetchStarshipsById(id): Observable<any> {
    return this.apiService.get(httpStarshipById(id));
  }
}
