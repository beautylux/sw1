import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from 'app/shared/services/api/api.service';

import { httpSpeciesList, httpSpecieById } from 'app/shared/utils/http.helper';

@Injectable({
  providedIn: 'root'
})
export class SpeciesService {

  constructor(
    private apiService: ApiService
  ) { }

  fetchSpeciesList(): Observable<any> {
    return this.apiService.get(httpSpeciesList());
  }

  fetchSpeciesId(id): Observable<any> {
    return this.apiService.get(httpSpecieById(id));
  }
}
