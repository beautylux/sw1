import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from 'app/shared/services/api/api.service';

import {httpVehicleById, httpVehiclesList} from 'app/shared/utils/http.helper';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  constructor(
    private apiService: ApiService
  ) { }

  fetchVehiclesList(): Observable<any> {
    return this.apiService.get(httpVehiclesList());
  }

  fetchVehiclesId(id): Observable<any> {
    return this.apiService.get(httpVehicleById(id));
  }
}
