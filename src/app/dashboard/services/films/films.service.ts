import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from 'app/shared/services/api/api.service';

import {httpFilmsList, httpFilmById} from 'app/shared/utils/http.helper';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(
    private apiService: ApiService
  ) { }

  fetchFilmsList(): Observable<any> {
    return this.apiService.get(httpFilmsList());
  }
  fetchFilmseId(id): Observable<any> {
    return this.apiService.get(httpFilmById(id));
  }
}
