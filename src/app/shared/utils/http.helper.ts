// tslint:disable: max-line-length

/**
 * Peoples
 * ----------------------------------------------
 */
export const httpPeopleList = () => 'people';
export const httpPeopleById = (id) => `people/${id}`;

/**
 * Starships
 * ----------------------------------------------
 */
export const httpStarshipsList = () => 'starships';
export const httpStarshipById = (id) => `starships/${id}`;

/**
 * Planets
 * ----------------------------------------------
 */
export const httpPlanetsList = () => 'planets';
export const httpPlanetById = (id) => `planets/${id}`;

/**
 * Films
 * ----------------------------------------------
 */
export const httpFilmsList = () => 'films';
export const httpFilmById = (id) => `films/${id}`;

/**
 * Species
 * ----------------------------------------------
 */
export const httpSpeciesList = () => 'species';
export const httpSpecieById = (id) => `species/${id}`;

/**
 * Vehicles
 * ----------------------------------------------
 */
export const httpVehiclesList = () => 'vehicles';
export const httpVehicleById = (id) => `vehicles/${id}`;
