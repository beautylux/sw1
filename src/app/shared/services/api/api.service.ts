import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  api: string;
  error: string;

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    this.api = environment.apiHost;
  }

  get(path: string, options: object = {}, ownDomain: boolean = false): Observable<any> {
    const url = ownDomain ? '' : this.api;

    return this.http.get(`${url}${path}`, { ...options })
      .pipe(
        map((response: any) => response && response.results ? response.results : response),
        catchError(this.handleError(path))
      );
  }

  put(path: string, body: object = {}): Observable<any> {
    return this.http.put(
      `${this.api}${path}`,
      JSON.stringify(body)
    ).pipe(
      map(data => data === null ? true : data), // return true if we get succeeded empty content
      catchError(this.handleError(path))
    );
  }

  post(path: string, body: object = {}): Observable<any> {
    return this.http.post(
      `${this.api}${path}`,
      JSON.stringify(body)
    ).pipe(
      map(data => data === null ? true : data), // return true if we get succeeded empty content
      catchError(this.handleError(path))
    );
  }

  patch(path: string, body: object = {}): Observable<any> {
    return this.http.patch(
      `${this.api}${path}`,
      JSON.stringify(body)
    ).pipe(
      map(data => data === null ? true : data), // return true if we get succeeded empty content
      catchError(this.handleError(path))
    );
  }

  delete(path: string): Observable<any> {
    return this.http.delete(
      `${this.api}${path}`
    ).pipe(
      map(data => data === null ? true : data), // return true if we get succeeded empty content
      catchError(this.handleError(path))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation: string = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      // console.error(error); // log to console instead

      switch (error.status) {
        case 503:
          this.router.navigate(['maintenance']);
          // @todo use store
          return throwError(error);

        default:
          this.log(`${operation} failed: ${error.message}`);
          // Let the app keep running by returning an empty result.
          // return of(result as T);
          return throwError(error);
      }
    };
  }

  private log(message: string): void {
    console.error('Error', message);
  }
}
