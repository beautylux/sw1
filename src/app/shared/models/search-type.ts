export enum SearchType {
  People,
  Films,
  Planets,
  Species,
  Starships,
  Vehicles
}
